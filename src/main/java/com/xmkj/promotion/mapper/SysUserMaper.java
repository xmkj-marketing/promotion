package com.xmkj.promotion.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xmkj.promotion.entity.SysUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysUserMaper extends BaseMapper<SysUser> {
    
}
