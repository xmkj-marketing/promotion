package com.xmkj.promotion.mapper.coupon;

import com.xmkj.promotion.entity.coupon.PromotionCouponScope;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 优惠券商品作用范围表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionCouponScopeMapper extends BaseMapper<PromotionCouponScope> {

}
