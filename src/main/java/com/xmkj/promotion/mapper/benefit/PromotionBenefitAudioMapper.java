package com.xmkj.promotion.mapper.benefit;

import com.xmkj.promotion.entity.benefit.PromotionBenefitAudio;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 线下优惠审核开关表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionBenefitAudioMapper extends BaseMapper<PromotionBenefitAudio> {

}
