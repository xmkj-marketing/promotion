package com.xmkj.promotion.mapper.voucher;

import com.xmkj.promotion.entity.voucher.PromotionVoucherDis;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 抵用券分发表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionVoucherDisMapper extends BaseMapper<PromotionVoucherDis> {

}
