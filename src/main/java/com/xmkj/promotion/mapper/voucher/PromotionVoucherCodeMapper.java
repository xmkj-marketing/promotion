package com.xmkj.promotion.mapper.voucher;

import com.xmkj.promotion.entity.voucher.PromotionVoucherCode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 抵用券历史记录表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionVoucherCodeMapper extends BaseMapper<PromotionVoucherCode> {

}
