package com.xmkj.promotion.mapper.basic;

import com.xmkj.promotion.entity.basic.PromotionGroupon;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 团购表表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionGrouponMapper extends BaseMapper<PromotionGroupon> {

}
