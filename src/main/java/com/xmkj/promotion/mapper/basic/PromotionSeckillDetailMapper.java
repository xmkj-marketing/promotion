package com.xmkj.promotion.mapper.basic;

import com.xmkj.promotion.entity.basic.PromotionSeckillDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 秒杀详情表表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionSeckillDetailMapper extends BaseMapper<PromotionSeckillDetail> {

}
