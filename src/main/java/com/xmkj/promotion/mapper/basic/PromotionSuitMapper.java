package com.xmkj.promotion.mapper.basic;

import com.xmkj.promotion.entity.basic.PromotionSuit;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 套装促销表表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionSuitMapper extends BaseMapper<PromotionSuit> {

}
