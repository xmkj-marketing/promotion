package com.xmkj.promotion.mapper.basic;

import com.xmkj.promotion.entity.basic.PromotionAdditionalMoney;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 促销加价购表表 Mapper 接口
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Mapper
public interface PromotionAdditionalMoneyMapper extends BaseMapper<PromotionAdditionalMoney> {

}
