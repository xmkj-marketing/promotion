package com.xmkj.promotion.service.voucher;

import com.xmkj.promotion.entity.voucher.PromotionVoucherInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 抵用券表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionVoucherInfoService extends IService<PromotionVoucherInfo> {

}
