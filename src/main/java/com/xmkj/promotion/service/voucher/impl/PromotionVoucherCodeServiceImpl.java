package com.xmkj.promotion.service.voucher.impl;

import com.xmkj.promotion.entity.voucher.PromotionVoucherCode;
import com.xmkj.promotion.mapper.voucher.PromotionVoucherCodeMapper;
import com.xmkj.promotion.service.voucher.PromotionVoucherCodeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抵用券历史记录表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionVoucherCodeServiceImpl extends ServiceImpl<PromotionVoucherCodeMapper, PromotionVoucherCode> implements PromotionVoucherCodeService {

}
