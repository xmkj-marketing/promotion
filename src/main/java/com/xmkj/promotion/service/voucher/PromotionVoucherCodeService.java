package com.xmkj.promotion.service.voucher;

import com.xmkj.promotion.entity.voucher.PromotionVoucherCode;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 抵用券历史记录表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionVoucherCodeService extends IService<PromotionVoucherCode> {

}
