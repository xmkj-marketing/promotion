package com.xmkj.promotion.service.voucher.impl;

import com.xmkj.promotion.entity.voucher.PromotionVoucherInfo;
import com.xmkj.promotion.mapper.voucher.PromotionVoucherInfoMapper;
import com.xmkj.promotion.service.voucher.PromotionVoucherInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抵用券表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionVoucherInfoServiceImpl extends ServiceImpl<PromotionVoucherInfoMapper, PromotionVoucherInfo> implements PromotionVoucherInfoService {

}
