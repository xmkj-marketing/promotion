package com.xmkj.promotion.service.voucher.impl;

import com.xmkj.promotion.entity.voucher.PromotionVoucherDis;
import com.xmkj.promotion.mapper.voucher.PromotionVoucherDisMapper;
import com.xmkj.promotion.service.voucher.PromotionVoucherDisService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抵用券分发表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionVoucherDisServiceImpl extends ServiceImpl<PromotionVoucherDisMapper, PromotionVoucherDis> implements PromotionVoucherDisService {

}
