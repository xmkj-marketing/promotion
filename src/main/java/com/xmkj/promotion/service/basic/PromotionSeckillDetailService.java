package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionSeckillDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 秒杀详情表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionSeckillDetailService extends IService<PromotionSeckillDetail> {

}
