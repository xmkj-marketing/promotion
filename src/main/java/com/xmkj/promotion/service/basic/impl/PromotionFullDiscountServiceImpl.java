package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionFullDiscount;
import com.xmkj.promotion.mapper.basic.PromotionFullDiscountMapper;
import com.xmkj.promotion.service.basic.PromotionFullDiscountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销满折表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionFullDiscountServiceImpl extends ServiceImpl<PromotionFullDiscountMapper, PromotionFullDiscount> implements PromotionFullDiscountService {

}
