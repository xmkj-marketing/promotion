package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionAdditionalMoneyDetail;
import com.xmkj.promotion.mapper.basic.PromotionAdditionalMoneyDetailMapper;
import com.xmkj.promotion.service.basic.PromotionAdditionalMoneyDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 加购商品表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionAdditionalMoneyDetailServiceImpl extends ServiceImpl<PromotionAdditionalMoneyDetailMapper, PromotionAdditionalMoneyDetail> implements PromotionAdditionalMoneyDetailService {

}
