package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionRush;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 促销限时抢购表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionRushService extends IService<PromotionRush> {

}
