package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionRush;
import com.xmkj.promotion.mapper.basic.PromotionRushMapper;
import com.xmkj.promotion.service.basic.PromotionRushService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销限时抢购表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionRushServiceImpl extends ServiceImpl<PromotionRushMapper, PromotionRush> implements PromotionRushService {

}
