package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionGroupon;
import com.xmkj.promotion.mapper.basic.PromotionGrouponMapper;
import com.xmkj.promotion.service.basic.PromotionGrouponService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 团购表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionGrouponServiceImpl extends ServiceImpl<PromotionGrouponMapper, PromotionGroupon> implements PromotionGrouponService {

}
