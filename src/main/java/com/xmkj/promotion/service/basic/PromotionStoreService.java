package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionStore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 促销店铺范围表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionStoreService extends IService<PromotionStore> {

}
