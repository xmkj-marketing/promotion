package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionGrouponGoods;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 团购商品表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionGrouponGoodsService extends IService<PromotionGrouponGoods> {

}
