package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionAudit;
import com.xmkj.promotion.mapper.basic.PromotionAuditMapper;
import com.xmkj.promotion.service.basic.PromotionAuditService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销活动审核表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionAuditServiceImpl extends ServiceImpl<PromotionAuditMapper, PromotionAudit> implements PromotionAuditService {

}
