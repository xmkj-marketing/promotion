package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionAdditionalMoneyDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 加购商品表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionAdditionalMoneyDetailService extends IService<PromotionAdditionalMoneyDetail> {

}
