package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionPresell;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 促销预售表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionPresellService extends IService<PromotionPresell> {

}
