package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionFullGiftDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 满赠赠品表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionFullGiftDetailService extends IService<PromotionFullGiftDetail> {

}
