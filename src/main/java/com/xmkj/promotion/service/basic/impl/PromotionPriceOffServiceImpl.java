package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionPriceOff;
import com.xmkj.promotion.mapper.basic.PromotionPriceOffMapper;
import com.xmkj.promotion.service.basic.PromotionPriceOffService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销直降表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionPriceOffServiceImpl extends ServiceImpl<PromotionPriceOffMapper, PromotionPriceOff> implements PromotionPriceOffService {

}
