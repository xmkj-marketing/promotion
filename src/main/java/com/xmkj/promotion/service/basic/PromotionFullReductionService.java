package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionFullReduction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 促销满减表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionFullReductionService extends IService<PromotionFullReduction> {

}
