package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionFullGift;
import com.xmkj.promotion.mapper.basic.PromotionFullGiftMapper;
import com.xmkj.promotion.service.basic.PromotionFullGiftService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销满赠表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionFullGiftServiceImpl extends ServiceImpl<PromotionFullGiftMapper, PromotionFullGift> implements PromotionFullGiftService {

}
