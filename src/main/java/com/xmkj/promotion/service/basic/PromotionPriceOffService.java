package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionPriceOff;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 促销直降表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionPriceOffService extends IService<PromotionPriceOff> {

}
