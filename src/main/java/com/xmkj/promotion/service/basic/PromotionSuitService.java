package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionSuit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套装促销表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionSuitService extends IService<PromotionSuit> {

}
