package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionFullReduction;
import com.xmkj.promotion.mapper.basic.PromotionFullReductionMapper;
import com.xmkj.promotion.service.basic.PromotionFullReductionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销满减表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionFullReductionServiceImpl extends ServiceImpl<PromotionFullReductionMapper, PromotionFullReduction> implements PromotionFullReductionService {

}
