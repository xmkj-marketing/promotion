package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionGoods;
import com.xmkj.promotion.mapper.basic.PromotionGoodsMapper;
import com.xmkj.promotion.service.basic.PromotionGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销商品范围表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionGoodsServiceImpl extends ServiceImpl<PromotionGoodsMapper, PromotionGoods> implements PromotionGoodsService {

}
