package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionSuitDetail;
import com.xmkj.promotion.mapper.basic.PromotionSuitDetailMapper;
import com.xmkj.promotion.service.basic.PromotionSuitDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 套装明细表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionSuitDetailServiceImpl extends ServiceImpl<PromotionSuitDetailMapper, PromotionSuitDetail> implements PromotionSuitDetailService {

}
