package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionSeckill;
import com.xmkj.promotion.mapper.basic.PromotionSeckillMapper;
import com.xmkj.promotion.service.basic.PromotionSeckillService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销秒杀表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionSeckillServiceImpl extends ServiceImpl<PromotionSeckillMapper, PromotionSeckill> implements PromotionSeckillService {

}
