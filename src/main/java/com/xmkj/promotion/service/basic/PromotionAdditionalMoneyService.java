package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionAdditionalMoney;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 促销加价购表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionAdditionalMoneyService extends IService<PromotionAdditionalMoney> {

}
