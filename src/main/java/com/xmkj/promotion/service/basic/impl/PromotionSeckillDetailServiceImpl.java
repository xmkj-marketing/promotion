package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionSeckillDetail;
import com.xmkj.promotion.mapper.basic.PromotionSeckillDetailMapper;
import com.xmkj.promotion.service.basic.PromotionSeckillDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 秒杀详情表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionSeckillDetailServiceImpl extends ServiceImpl<PromotionSeckillDetailMapper, PromotionSeckillDetail> implements PromotionSeckillDetailService {

}
