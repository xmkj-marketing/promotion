package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionSuit;
import com.xmkj.promotion.mapper.basic.PromotionSuitMapper;
import com.xmkj.promotion.service.basic.PromotionSuitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 套装促销表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionSuitServiceImpl extends ServiceImpl<PromotionSuitMapper, PromotionSuit> implements PromotionSuitService {

}
