package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionGrouponGoods;
import com.xmkj.promotion.mapper.basic.PromotionGrouponGoodsMapper;
import com.xmkj.promotion.service.basic.PromotionGrouponGoodsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 团购商品表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionGrouponGoodsServiceImpl extends ServiceImpl<PromotionGrouponGoodsMapper, PromotionGrouponGoods> implements PromotionGrouponGoodsService {

}
