package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionStore;
import com.xmkj.promotion.mapper.basic.PromotionStoreMapper;
import com.xmkj.promotion.service.basic.PromotionStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销店铺范围表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionStoreServiceImpl extends ServiceImpl<PromotionStoreMapper, PromotionStore> implements PromotionStoreService {

}
