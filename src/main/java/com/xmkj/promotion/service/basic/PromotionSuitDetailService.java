package com.xmkj.promotion.service.basic;

import com.xmkj.promotion.entity.basic.PromotionSuitDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 套装明细表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionSuitDetailService extends IService<PromotionSuitDetail> {

}
