package com.xmkj.promotion.service.basic.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xmkj.promotion.dto.sysuser.SysUserAddDTO;
import com.xmkj.promotion.entity.SysUser;
import com.xmkj.promotion.enums.StatusEnum;
import com.xmkj.promotion.mapper.SysUserMaper;
import com.xmkj.promotion.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMaper,SysUser> implements SysUserService {

    @Autowired
    private SysUserMaper sysUserMaper;

    @Override
    public SysUser saveUser(SysUserAddDTO addDTO) {
        SysUser user = new SysUser();
        user.setId("43669651263621635");
        user.setPassword(addDTO.getPassword());
        user.setUsername(addDTO.getUsername());
        user.setCompanyId(addDTO.getCompanyId());
        user.setCreateBy("admin");
        user.setStatus(StatusEnum.NOMRAL);
        sysUserMaper.insert(user);
        return user;
    }

    @Override
    public SysUser getSysUserById(String id) {
        SysUser user = sysUserMaper.selectById(id);
        System.out.println(JSON.toJSONString(user));
        System.out.println(user);
        System.out.println(JSON.toJSON(user));
        return user;
    }
}
