package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionAdditionalMoney;
import com.xmkj.promotion.mapper.basic.PromotionAdditionalMoneyMapper;
import com.xmkj.promotion.service.basic.PromotionAdditionalMoneyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销加价购表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionAdditionalMoneyServiceImpl extends ServiceImpl<PromotionAdditionalMoneyMapper, PromotionAdditionalMoney> implements PromotionAdditionalMoneyService {

}
