package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionActivity;
import com.xmkj.promotion.mapper.basic.PromotionActivityMapper;
import com.xmkj.promotion.service.basic.PromotionActivityService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 促销活动表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionActivityServiceImpl extends ServiceImpl<PromotionActivityMapper, PromotionActivity> implements PromotionActivityService {

}
