package com.xmkj.promotion.service.basic.impl;

import com.xmkj.promotion.entity.basic.PromotionFreeDelivery;
import com.xmkj.promotion.mapper.basic.PromotionFreeDeliveryMapper;
import com.xmkj.promotion.service.basic.PromotionFreeDeliveryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 包邮促销表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionFreeDeliveryServiceImpl extends ServiceImpl<PromotionFreeDeliveryMapper, PromotionFreeDelivery> implements PromotionFreeDeliveryService {

}
