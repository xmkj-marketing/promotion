package com.xmkj.promotion.service.coupon;

import com.xmkj.promotion.entity.coupon.PromotionCouponScope;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券商品作用范围表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionCouponScopeService extends IService<PromotionCouponScope> {

}
