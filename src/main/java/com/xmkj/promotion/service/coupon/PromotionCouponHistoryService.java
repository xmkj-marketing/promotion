package com.xmkj.promotion.service.coupon;

import com.xmkj.promotion.entity.coupon.PromotionCouponHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券历史记录表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionCouponHistoryService extends IService<PromotionCouponHistory> {

}
