package com.xmkj.promotion.service.coupon;

import com.xmkj.promotion.entity.coupon.PromotionCouponStore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券适用店铺信息表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionCouponStoreService extends IService<PromotionCouponStore> {

}
