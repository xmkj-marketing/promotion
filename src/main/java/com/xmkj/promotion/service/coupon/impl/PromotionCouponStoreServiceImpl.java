package com.xmkj.promotion.service.coupon.impl;

import com.xmkj.promotion.entity.coupon.PromotionCouponStore;
import com.xmkj.promotion.mapper.coupon.PromotionCouponStoreMapper;
import com.xmkj.promotion.service.coupon.PromotionCouponStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券适用店铺信息表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionCouponStoreServiceImpl extends ServiceImpl<PromotionCouponStoreMapper, PromotionCouponStore> implements PromotionCouponStoreService {

}
