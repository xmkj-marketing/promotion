package com.xmkj.promotion.service.coupon;

import com.xmkj.promotion.entity.coupon.PromotionCouponInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 优惠券信息表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionCouponInfoService extends IService<PromotionCouponInfo> {

}
