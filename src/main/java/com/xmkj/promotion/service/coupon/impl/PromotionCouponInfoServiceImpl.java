package com.xmkj.promotion.service.coupon.impl;

import com.xmkj.promotion.entity.coupon.PromotionCouponInfo;
import com.xmkj.promotion.mapper.coupon.PromotionCouponInfoMapper;
import com.xmkj.promotion.service.coupon.PromotionCouponInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券信息表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionCouponInfoServiceImpl extends ServiceImpl<PromotionCouponInfoMapper, PromotionCouponInfo> implements PromotionCouponInfoService {

}
