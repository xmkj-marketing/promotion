package com.xmkj.promotion.service.coupon.impl;

import com.xmkj.promotion.entity.coupon.PromotionCouponScope;
import com.xmkj.promotion.mapper.coupon.PromotionCouponScopeMapper;
import com.xmkj.promotion.service.coupon.PromotionCouponScopeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券商品作用范围表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionCouponScopeServiceImpl extends ServiceImpl<PromotionCouponScopeMapper, PromotionCouponScope> implements PromotionCouponScopeService {

}
