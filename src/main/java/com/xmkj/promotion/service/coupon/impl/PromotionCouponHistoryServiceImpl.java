package com.xmkj.promotion.service.coupon.impl;

import com.xmkj.promotion.entity.coupon.PromotionCouponHistory;
import com.xmkj.promotion.mapper.coupon.PromotionCouponHistoryMapper;
import com.xmkj.promotion.service.coupon.PromotionCouponHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 优惠券历史记录表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionCouponHistoryServiceImpl extends ServiceImpl<PromotionCouponHistoryMapper, PromotionCouponHistory> implements PromotionCouponHistoryService {

}
