package com.xmkj.promotion.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.xmkj.promotion.dto.sysuser.SysUserAddDTO;
import com.xmkj.promotion.entity.SysUser;

public interface SysUserService extends IService<SysUser>{

    SysUser saveUser(SysUserAddDTO addDTO);

    SysUser getSysUserById(String id);


}
