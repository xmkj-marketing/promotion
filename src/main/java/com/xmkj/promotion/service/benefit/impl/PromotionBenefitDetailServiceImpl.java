package com.xmkj.promotion.service.benefit.impl;

import com.xmkj.promotion.entity.benefit.PromotionBenefitDetail;
import com.xmkj.promotion.mapper.benefit.PromotionBenefitDetailMapper;
import com.xmkj.promotion.service.benefit.PromotionBenefitDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线下优惠详情表表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionBenefitDetailServiceImpl extends ServiceImpl<PromotionBenefitDetailMapper, PromotionBenefitDetail> implements PromotionBenefitDetailService {

}
