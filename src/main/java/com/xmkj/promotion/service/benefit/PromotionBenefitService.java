package com.xmkj.promotion.service.benefit;

import com.xmkj.promotion.entity.benefit.PromotionBenefit;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线下优惠申请表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionBenefitService extends IService<PromotionBenefit> {

}
