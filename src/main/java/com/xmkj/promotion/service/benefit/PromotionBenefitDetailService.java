package com.xmkj.promotion.service.benefit;

import com.xmkj.promotion.entity.benefit.PromotionBenefitDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线下优惠详情表表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionBenefitDetailService extends IService<PromotionBenefitDetail> {

}
