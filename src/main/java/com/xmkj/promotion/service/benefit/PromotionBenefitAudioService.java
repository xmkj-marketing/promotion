package com.xmkj.promotion.service.benefit;

import com.xmkj.promotion.entity.benefit.PromotionBenefitAudio;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 线下优惠审核开关表 服务类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
public interface PromotionBenefitAudioService extends IService<PromotionBenefitAudio> {

}
