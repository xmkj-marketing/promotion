package com.xmkj.promotion.service.benefit.impl;

import com.xmkj.promotion.entity.benefit.PromotionBenefit;
import com.xmkj.promotion.mapper.benefit.PromotionBenefitMapper;
import com.xmkj.promotion.service.benefit.PromotionBenefitService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线下优惠申请表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionBenefitServiceImpl extends ServiceImpl<PromotionBenefitMapper, PromotionBenefit> implements PromotionBenefitService {

}
