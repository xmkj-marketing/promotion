package com.xmkj.promotion.service.benefit.impl;

import com.xmkj.promotion.entity.benefit.PromotionBenefitAudio;
import com.xmkj.promotion.mapper.benefit.PromotionBenefitAudioMapper;
import com.xmkj.promotion.service.benefit.PromotionBenefitAudioService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 线下优惠审核开关表 服务实现类
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Service
public class PromotionBenefitAudioServiceImpl extends ServiceImpl<PromotionBenefitAudioMapper, PromotionBenefitAudio> implements PromotionBenefitAudioService {

}
