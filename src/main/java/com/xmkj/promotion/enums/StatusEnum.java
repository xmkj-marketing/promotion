package com.xmkj.promotion.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.baomidou.mybatisplus.annotation.IEnum;
import lombok.Getter;
import org.omg.CORBA.PUBLIC_MEMBER;

@Getter
public enum StatusEnum implements IEnum<Integer> {

    NOMRAL(0,"正常"),
    FREEZE(1,"拉黑");

    @EnumValue
    private Integer code;
    private String status;

    StatusEnum(int code,String status){
        this.code = code;
        this.status = status;
    }

    public static StatusEnum valueOf(int code){
        for (StatusEnum item:values()){
            if (item.getCode() == code){
                return item;
            }
        }
        return null;
    }

    @Override
    public String toString(){
        return this.status;
    }

    @Override
    public Integer getValue() {
        return code;
    }
}
