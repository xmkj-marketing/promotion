package com.xmkj.promotion.dto.sysuser;

import com.xmkj.promotion.enums.StatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SysUserAddDTO {

    private String createBy;
    private Integer delFlag;
    private String updateBy;
    private String avatar;
    private String companyId;
    private String nickName;
    private String password;
    private String phone;
    private StatusEnum status;
    private String username;
}
