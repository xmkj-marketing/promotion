package com.xmkj.promotion.entity.voucher;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 抵用券分发表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionVoucherDis extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 抵用券来源
     */
    private String disSourceId;

    /**
     * 分发总数量
     */
    private Integer disSum;

    /**
     * 剩余总数量
     */
    private Integer remainSum;

    /**
     * 导购员ID
     */
    private String salesId;

    /**
     * 分销商ID
     */
    private String storeId;

    /**
     * 抵用券ID
     */
    private String voucherId;


}
