package com.xmkj.promotion.entity.voucher;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 抵用券表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionVoucherInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 审核说明
     */
    private String auditDes;

    /**
     * 审核人
     */
    private String auditDy;

    /**
     * 审核状态 0：待审核 1：审核通过 2：审核未通过
     */
    private Integer auditStatus;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 备用字段1
     */
    private String field1;

    /**
     * 备用字段2
     */
    private String field2;

    /**
     * 抵扣金额
     */
    private BigDecimal reducePrice;

    /**
     * 抵用券剩余数量
     */
    private Integer remainNum;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 总部ID
     */
    private String storeInfo;

    /**
     * 抵用券名称
     */
    private String voucherName;

    /**
     * 抵用券总数量
     */
    private Integer voucherNum;

    /**
     * 状态 0：未开始 1：进行中 2：已结束
     */
    private Integer voucherStatus;

    /**
     * 品牌ID
     */
    private String brandId;


}
