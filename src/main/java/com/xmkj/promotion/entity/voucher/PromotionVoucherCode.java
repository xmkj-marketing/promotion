package com.xmkj.promotion.entity.voucher;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 抵用券历史记录表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionVoucherCode extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 店铺ID
     */
    private Date acquireTime;

    /**
     * 店铺ID
     */
    private String orderId;

    /**
     * 店铺ID
     */
    private String storeId;

    /**
     * 撤销时间
     */
    private Date undoTime;

    /**
     * 使用数量
     */
    private Integer useNumber;

    /**
     * 店铺ID
     */
    private Integer useStatus;

    /**
     * 抵用券ID
     */
    private String voucherId;


}
