package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销预售表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionPresell extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 尾款结束支付时间
     */
    private Date balancePaymentEndTime;

    /**
     * 尾款开始支付时间
     */
    private Date balancePaymentStartTime;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 完成支付几天后发货
     */
    private Integer deliveryDays;

    /**
     * 发货时间
     */
    private Date deliveryTime;

    /**
     * 发货时间类型 0deliveryTime 1deliveryDays
     */
    private Integer deliveryType;

    /**
     * 定金
     */
    private BigDecimal earent;

    /**
     * 定金结束支付时间
     */
    private Date earnestEndTime;

    /**
     * 定金开始支付时间
     */
    private Date earnestStartTime;

    /**
     * 定金膨胀倍数
     */
    private Integer expansionMultiple;

    /**
     * 活动后商品状态 0上架 1下架
     */
    private Integer goodsStatus;

    /**
     * 预售类型 110全款预售 111定金预售
     */
    private Integer presellType;

    /**
     * 促销ID
     */
    private String promotionId;


}
