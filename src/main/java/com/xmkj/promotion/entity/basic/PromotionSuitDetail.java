package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 套装明细表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionSuitDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 商品详情ID
     */
    private String goodsDetailId;

    /**
     * 商品数量
     */
    private Integer goodsNum;

    /**
     * 是否为主商品:0是，1不是
     */
    private Integer isMajor;

    /**
     * 套装ID
     */
    private String suitId;

    /**
     * 搭配价
     */
    private BigDecimal suitPrice;

    /**
     * 商品详情ID（全部商品时为0）
     */
    private String goodsId;


}
