package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 套装促销表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionSuit extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 一口价
     */
    private BigDecimal buyoutPrice;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 促销ID
     */
    private String promotionId;

    /**
     * 套装类型 1特惠套装 2人气组合
     */
    private Integer suitType;


}
