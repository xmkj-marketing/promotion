package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 包邮促销表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionFreeDelivery extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 包邮区域(json格式存市ID)
     */
    private String freeDeliveryArea;

    /**
     * 包邮范围 1整单包邮 2仅活动商品包邮
     */
    private Integer freeDeliveryScope;

    /**
     * 满多少件
     */
    private Integer fullAmount;

    /**
     * 满多少钱
     */
    private BigDecimal fullPrice;

    /**
     * 促销ID
     */
    private String promotionId;


}
