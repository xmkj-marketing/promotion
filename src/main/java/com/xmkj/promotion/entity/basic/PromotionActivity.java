package com.xmkj.promotion.entity.basic;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销活动表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class PromotionActivity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 审核人ID
     */
    private String auditBy;

    /**
     * 审核说明
     */
    private String auditDes;

    /**
     * 审核状态 0:待审核 1:审核通过 2:审核不通过
     */
    private Integer auditStatus;

    /**
     * 审核时间
     */
    private Date auditTime;

    /**
     * 开始时间
     */
    private Date beginTime;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 备用字段1
     */
    private String field1;

    /**
     * 备用字段2
     */
    private String field2;

    /**
     * 是否互斥 0:否 1:是
     */
    private Integer isActivityType;

    /**
     * 参与人群
     */
    private String joinLevel;

    /**
     * 叠加类型
     */
    private String mutualType;

    /**
     * 促销说明
     */
    private String promotionDes;

    /**
     * 促销图片
     */
    private String promotionImage;

    /**
     * 促销名称
     */
    private String promotionName;

    /**
     * 活动状态 0:未开始 1:进行中 2:已结束 3:以取消
     */
    private Integer promotionStatus;

    /**
     * 促销类型 1直降 2满减 3满折 4满赠 5加价购 6包邮 7套装 8限时抢购 9秒杀 10团购 11预售
     */
    private Integer promotionType;

    /**
     * 促销方式 0:无条件 1:满金额 2:满数量
     */
    private Integer promotionWay;

    /**
     * 适用平台
     */
    private String siteFlag;

    /**
     * 发起活动分销商(品牌)ID
     */
    private String storeId;

    /**
     * 活动品牌ID
     */
    private String brandId;


}
