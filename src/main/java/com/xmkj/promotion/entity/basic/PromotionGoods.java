package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销商品范围表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionGoods extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 促销商品ID（全部商品时为0，自定义商品时为goods_id）
     */
    private String goodsId;

    /**
     * 促销数量
     */
    private BigDecimal goodsNum;

    /**
     * 范围类型 0:全部商品 1:自定义商品
     */
    private Integer goodsType;

    /**
     * 促销ID
     */
    private String promotionId;


}
