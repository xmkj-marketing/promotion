package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 加购商品表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionAdditionalMoneyDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 换购价格
     */
    private BigDecimal additionalMoney;

    /**
     * 加购ID
     */
    private String additionalMoneyId;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 商品详情ID（skuID）
     */
    private String goodsDetailId;


}
