package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销加价购表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionAdditionalMoney extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 换购类型 0可换购一件 1可换购多件
     */
    private Integer exchangeType;

    /**
     * 满多少件
     */
    private Integer fullAmount;

    /**
     * 满多少钱
     */
    private BigDecimal fullPrice;

    /**
     * 促销ID
     */
    private String promotionId;


}
