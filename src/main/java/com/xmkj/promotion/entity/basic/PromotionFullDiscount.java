package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销满折表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionFullDiscount extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 满多少件
     */
    private Integer fullAmount;

    /**
     * 打多少折(输入0-10之间的数字，可以2位小数)
     */
    private BigDecimal fullDiscount;

    /**
     * 满多少钱
     */
    private BigDecimal fullPrice;

    /**
     * 促销ID
     */
    private String promotionId;

    /**
     * 是否抹零 0不抹 1自动抹分 2自动抹角抹分
     */
    private Integer roundOffType;


}
