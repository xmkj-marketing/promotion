package com.xmkj.promotion.entity.basic;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 团购商品表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionGrouponGoods extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 商品详情ID
     */
    private String goodsDetailId;

    /**
     * 团购ID
     */
    private String grouponId;

    /**
     * 团购第一阶梯价
     */
    private BigDecimal grouponPriceOne;

    /**
     * 团购第三阶梯价
     */
    private BigDecimal grouponPriceThree;

    /**
     * 团购第二阶梯价
     */
    private BigDecimal grouponPriceTwo;


}
