package com.xmkj.promotion.entity.basic;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销秒杀表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionSeckill extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 是否限购：0是，1不限制
     */
    private Integer isLimit;

    /**
     * 促销ID
     */
    private String promotionId;

    /**
     * 订单取消时间(按分钟)
     */
    private Integer seckillCancelTime;

    /**
     * ID限购数量
     */
    private Integer seckillLimitCount;


}
