package com.xmkj.promotion.entity.basic;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 促销限时抢购表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionRush extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 活动预告：0不预告，1创建成功开始预告，2开始前几个小时预告
     */
    private Integer activePreview;

    /**
     * 提前预告小时数
     */
    private Integer advanceHour;

    /**
     * 倒计时小时数
     */
    private Integer countDownHour;

    /**
     * 倒计时类型：0按活动倒计时，1自定义倒计时
     */
    private Integer countDownType;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 限购设置：0不限购，1限购数量，2仅前几件能折扣
     */
    private Integer limitType;

    /**
     * 促销ID
     */
    private String promotionId;

    /**
     * 是否抹零 0不抹 1自动抹分 2自动抹角抹分
     */
    private Integer roundOffType;

    /**
     * ID限购数量:表示限购数量或者享受折扣的数量
     */
    private Integer rushLimitCount;


}
