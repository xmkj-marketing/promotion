package com.xmkj.promotion.entity.basic;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 团购表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionGroupon extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 活动总限购数量
     */
    private Integer activityLimitCount;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 每单限购数量
     */
    private Integer eachLimitCount;

    /**
     * 团购有效期（按分钟）
     */
    private Integer grouponPeriod;

    /**
     * 团购类型 1普通拼团 2阶梯拼团 
     */
    private Integer grouponType;

    /**
     * 是否限购：0是，1不限制
     */
    private Integer isLimit;

    /**
     * 第一阶梯人数
     */
    private Integer participateNumber1;

    /**
     * 第二阶梯人数
     */
    private Integer participateNumber2;

    /**
     * 第三阶梯人数
     */
    private Integer participateNumber3;

    /**
     * 促销ID
     */
    private String promotionId;


}
