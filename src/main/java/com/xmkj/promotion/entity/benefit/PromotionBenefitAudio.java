package com.xmkj.promotion.entity.benefit;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 线下优惠审核开关表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionBenefitAudio extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 开关 0关 1开
     */
    private Integer audio;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 店铺ID
     */
    private String storeId;


}
