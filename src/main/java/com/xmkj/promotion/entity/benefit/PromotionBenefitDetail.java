package com.xmkj.promotion.entity.benefit;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 线下优惠详情表表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionBenefitDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 线下优惠ID
     */
    private String benefitId;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 商品详情ID
     */
    private String goodsDetailId;

    /**
     * 商品名称
     */
    private String goodsName;

    /**
     * 商品数量
     */
    private BigDecimal goodsNums;

    /**
     * 优惠后价格
     */
    private BigDecimal newPrice;

    /**
     * 原始价格
     */
    private BigDecimal oldPrice;


}
