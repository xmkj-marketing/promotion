package com.xmkj.promotion.entity.benefit;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 线下优惠申请表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionBenefit extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 审批意见
     */
    private String auditDes;

    /**
     * 优惠类型 1:整单优惠 2:单品优惠
     */
    private Integer benefitType;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 是否生效 0否 1是
     */
    private Integer isEffect;

    /**
     * 优惠金额
     */
    private BigDecimal money;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 原因
     */
    private String reason;

    /**
     * 优惠申请状态 0待审核 1通过 2拒绝 3撤回
     */
    private Integer status;

    /**
     * 门店ID(分销商ID)
     */
    private String storeId;

    /**
     * 订单编号
     */
    private String orderCode;


}
