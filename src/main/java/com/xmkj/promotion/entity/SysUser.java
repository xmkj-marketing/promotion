package com.xmkj.promotion.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.baomidou.mybatisplus.annotation.*;
import com.xmkj.promotion.enums.StatusEnum;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@TableName(value = "sys_user")
public class SysUser implements Serializable {
    private static final long serialVersionUID = 1L;


    @TableId(value = "id",type = IdType.INPUT)
    private String id;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    private String createBy;
    private Integer delFlag;
    private String updateBy;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    private String avatar;
    private String companyId;
    private String nickName;
    private String password;
    private String phone;
    private StatusEnum status;
    private String username;

    @Override
    public String toString(){
        return "{id:"+getId()+",password:"+getPassword()+",status:"+getStatus().toString()+"}";
    }

}
