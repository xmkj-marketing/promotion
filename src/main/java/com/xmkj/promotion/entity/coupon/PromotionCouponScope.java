package com.xmkj.promotion.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 优惠券商品作用范围表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionCouponScope extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 优惠券ID
     */
    private String couponId;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 范围ID 
     */
    private String scopeId;

    /**
     * 范围类型 0：全部商品 1：指定分类 2：指定商品
     */
    private Integer scopeType;


}
