package com.xmkj.promotion.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 优惠券适用店铺信息表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionCouponStore extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 优惠券ID
     */
    private String couponId;

    /**
     * 店铺Id
     */
    private String couponStoreId;

    /**
     * 店铺范围 0:全部店铺 1:指定店铺
     */
    private Integer couponStoreScope;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;


}
