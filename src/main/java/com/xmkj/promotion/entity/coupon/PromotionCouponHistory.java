package com.xmkj.promotion.entity.coupon;

import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 优惠券历史记录表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionCouponHistory extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 获得优惠券时间
     */
    private Date acquireTime;

    /**
     * 获取类型 0后台赠送 1主动获取
     */
    private Integer acquireType;

    /**
     * 优惠券券码
     */
    private String couponCode;

    /**
     * 优惠券ID
     */
    private String couponId;

    /**
     * 会员ID
     */
    private String customerId;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 领取人昵称
     */
    private String memberNickname;

    /**
     * 订单ID
     */
    private String orderId;

    /**
     * 使用状态 0未使用 1已使用 2已过期
     */
    private Integer useStatus;

    /**
     * 使用时间
     */
    private Date useTime;


}
