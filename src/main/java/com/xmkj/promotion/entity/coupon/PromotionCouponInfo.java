package com.xmkj.promotion.entity.coupon;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import com.xmkj.promotion.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 优惠券信息表
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class PromotionCouponInfo extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 审核人ID
     */
    private String auditBy;

    /**
     * 审核说明
     */
    private String auditDes;

    /**
     * 审核状态 0待审核 1 审核通过 2审核未通过
     */
    private Integer auditStatus;

    /**
     * 折扣力度
     */
    private BigDecimal couponAmount;

    /**
     * 优惠码
     */
    private String couponCode;

    /**
     * 优惠券名称
     */
    private String couponName;

    /**
     * 优惠面额
     */
    private BigDecimal couponPrice;

    /**
     * 状态 0未开始 1进行中 3已结束 4已取消
     */
    private Integer couponStatus;

    /**
     * 优惠券类型 0全场赠券 1会员赠券 2购物赠券 3注册赠券
     */
    private Integer couponType;

    /**
     * 删除人ID
     */
    private String deleteBy;

    /**
     * 删除时间
     */
    private Date deleteTime;

    /**
     * 发放方式 0可发放可领取 1仅可发放 2仅可领取
     */
    private Integer disWay;

    /**
     * 可以领取的结束日期
     */
    private Date enableEndTime;

    /**
     * 可以领取的开始日期
     */
    private Date enableStartTime;

    /**
     * 结束使用时间
     */
    private Date endTime;

    /**
     * 备用字段1
     */
    private String field1;

    /**
     * 备用字段2
     */
    private String field2;

    /**
     * 金额
     */
    private BigDecimal fullAmount;

    /**
     * 数量
     */
    private BigDecimal fullCount;

    /**
     * 满额类型 0无门槛 1满金额 2满数量
     */
    private Integer fullType;

    /**
     * 积分数 选择积分领取时，所需的积分数
     */
    private BigDecimal intergral;

    /**
     * 可领取的会员类型 0无限制
     */
    private String memberLimit;

    /**
     * 每人限领张数
     */
    private Integer perLimit;

    /**
     * 发行数量
     */
    private Integer publishCount;

    /**
     * 领取数量
     */
    private Integer receiveCount;

    /**
     * 领用方式 0:免费领取 1:积分领取 适用于全场增券、会员赠券等仅可以领取的优惠券
     */
    private Integer receiveWay;

    /**
     * 备注
     */
    private String remark;

    /**
     * 活动站点 0全部 1APP 2小程序 3PC
     */
    private Integer siteType;

    /**
     * 开始使用时间
     */
    private Date startTime;

    /**
     * 品牌、店铺、大区ID
     */
    private String storeId;

    /**
     * 已使用数量
     */
    private Integer useCount;

    /**
     * 领取当日起有效天数
     */
    private Integer validDays1;

    /**
     * 领取次日起有效天数
     */
    private Integer validDays2;

    /**
     * 时效 0绝对时效（xxx-xxx时间段有效）1领取当日起多少天有效 2领取次日起多少天有效
     */
    private Integer validType;

    /**
     * 品牌ID
     */
    private String brandId;


}
