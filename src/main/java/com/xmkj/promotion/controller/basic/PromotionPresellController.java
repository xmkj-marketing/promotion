package com.xmkj.promotion.controller.basic;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 促销预售表表 前端控制器
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@RestController
@RequestMapping("/promotion-presell")
public class PromotionPresellController {

}

