package com.xmkj.promotion.controller.basic;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 满赠赠品表 前端控制器
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@RestController
@RequestMapping("/promotion-full-gift-detail")
public class PromotionFullGiftDetailController {

}

