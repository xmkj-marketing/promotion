package com.xmkj.promotion.controller.voucher;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 抵用券表 前端控制器
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@RestController
@RequestMapping("/promotion-voucher-info")
public class PromotionVoucherInfoController {

}

