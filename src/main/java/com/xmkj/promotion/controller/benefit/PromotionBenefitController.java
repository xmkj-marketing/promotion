package com.xmkj.promotion.controller.benefit;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 线下优惠申请表 前端控制器
 * </p>
 *
 * @author Mr.liu
 * @since 2021-06-25
 */
@RestController
@RequestMapping("/promotion-benefit")
public class PromotionBenefitController {

}

