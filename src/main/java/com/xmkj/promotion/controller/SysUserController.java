package com.xmkj.promotion.controller;

import com.xmkj.promotion.common.ResultInfo;
import com.xmkj.promotion.dto.sysuser.SysUserAddDTO;
import com.xmkj.promotion.entity.SysUser;
import com.xmkj.promotion.service.SysUserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;

@Controller
@RequestMapping(value = "/sysUser")
public class SysUserController {

    @Resource
    private SysUserService sysUserService;

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @ResponseBody
    public ResultInfo<SysUser> save(@RequestBody SysUserAddDTO addDTO){
        return new ResultInfo<SysUser>().result(sysUserService.saveUser(addDTO));
    }

    @RequestMapping(value = "/delete/{id}",method = RequestMethod.DELETE)
    @ResponseBody
    public ResultInfo<Boolean> delete(@RequestParam("id") String id){
        return new ResultInfo<Boolean>().result(sysUserService.removeById(id));
    }

    @RequestMapping(value = "/getById",method = RequestMethod.GET)
    @ResponseBody
    public ResultInfo<SysUser> getBy(@RequestParam(value = "id",required = true) String id){
        return new ResultInfo<SysUser>().result(sysUserService.getById(id));
    }

    @RequestMapping(value="/test",method = RequestMethod.GET)
    public ModelAndView test(){
        ModelAndView mv = new ModelAndView();
        SysUser user = sysUserService.getById("43669651263621635");
        mv.addObject("user",user);
        mv.setViewName("/main");
        return mv;
    }

}
